  var words = ["encyklopedia", "administratorka", "afroamerykanka", "unieruchomiony", "pszczelarz","radioaktywny",
    "rentgenowski"];
  var word;
  var chars;
  var i;
  var number_of_windows="";
  var windows2="";
  var alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
  var how_many_tries = 0 ;
  var good_discovery = 0 ;
  var info = $("#information");
  var startGame = $('#startGame');
  var answer = $('#answer');
  var selectedWord = $("#selectedWord");
  var keyboard = $("#keyboard");
  var numberOfMoves = $("#numberOfMoves");
  var restart = $("#restart");

  startGame.click(function(){
    var number= Math.floor(Math.random()*words.length);
    word=words[number];

    var word_length = word.length;
    info.html("Wylosowany wyraz ma " + word_length + " liter.<br>Podaj literę.");

    chars=word.split('');

    startGame.addClass('d-none');
    answer.removeClass('d-none');

    for (i=0;i<chars.length;i++){
      number_of_windows += '<span id="' + i + '" class="border border-primary">_</span>';
    }

    selectedWord.html(number_of_windows);
    for (i = 0 ; i < alphabet.length ; i++) {
      windows2 += '<span class="letter border border-danger">' + alphabet[i] + '</span>';
    }
    keyboard.html(windows2);
  });

  keyboard.on("click",'.letter', function(){
    how_many_tries++;
    numberOfMoves.html("Liczba ruchów : " + how_many_tries );
    var letter =$(this).html();
    for (i = 0 ; i < chars.length ; i++){
      if (chars[i]===letter){
        $("#" + i).html(letter);
        $(this).removeClass('letter');
        $(this).addClass('open');
        $(this).addClass('bg-success');
        good_discovery++;
        if (good_discovery ===chars.length){
          restart.removeClass('d-none');
          selectedWord.find('span').each(function () {
            $(this).addClass('bg-primary');
            $(this).addClass('letter_color');
          });
          setTimeout(function () {
            alert("Koniec gry. Twoja liczba ruchów to: " + how_many_tries + " ." );
          },500);
        }
      }else{
        $(this).removeClass('letter');
        $(this).addClass('open');
      }
    }
  });

  answer.on('change', function (){
    var password= answer.val();
    var i = 0;
    if (password===word){
      selectedWord.find('span').each(function () {
        $(this).html(chars[i]);
        i++;
        $(this).addClass('bg-primary');
        $(this).addClass('letter_color');
      });
      setTimeout(function () {
        alert("Koniec gry. Twoja liczba ruchów to: " + how_many_tries + " ." );
      },500);
      restart.removeClass('d-none');
      $(".letter").removeClass('letter');
    }else{
      answer.val("");
      alert("Błędne hasło");
    }
  });